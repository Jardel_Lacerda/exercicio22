import { Component } from "react";
import "./App.css";
import "./reset.css";

class App extends Component {
  state = {
    show: false,
    showH1: false,
    showh2: false,
    showDiv: false,
    ShowH3: false,
    ShowB: false,
  };

  hideButton = (item) => {
    this.setState({ [item]: false });
  };

  showButton = (item) => {
    this.setState({ [item]: true });
  };

  showAndHideButton = (item) => {
    this.setState({ [item]: !this.state[item] });
  };

  render() {
    return (
      <section>
        <div className="divStyle">
          <button onClick={() => this.hideButton("show")}>Esconda Div!</button>
          <button onClick={() => this.showButton("show")}>Mostrar Div!</button>
          {this.state.show && <p>Olá React!</p>}
        </div>

        <div className="divStyle">
          <button onClick={() => this.hideButton("showH1")}>
            Esconda Div!
          </button>
          <button onClick={() => this.showButton("showH1")}>
            Mostrar Div!
          </button>
          {this.state.showH1 && <h1>Olá, h1</h1>}
        </div>

        <div className="divStyle">
          <button onClick={() => this.hideButton("showDiv")}>
            Esconda Div!
          </button>
          <button onClick={() => this.showButton("showDiv")}>
            Mostrar Div!
          </button>
          {this.state.showDiv && <div>Olá Div</div>}
        </div>

        <div className="divStyle">
          <button onClick={() => this.hideButton("showH2")}>
            Esconda Div!
          </button>
          <button onClick={() => this.showButton("showH2")}>
            Mostrar Div!
          </button>
          {this.state.showH2 && <h2>Olá H2</h2>}
        </div>

        <div className="divStyle">
          <button onClick={() => this.showAndHideButton("ShowH3")}>
            Mostrar/Esconder
          </button>
          {this.state.ShowH3 && <h3>Olá H3</h3>}
        </div>

        <div className="divStyle">
          <button onClick={() => this.showAndHideButton("ShowB")}>
            Mostrar/Esconder
          </button>
          {this.state.ShowB && (
            <b>
              <br />
              Olá B
            </b>
          )}
        </div>
      </section>
    );
  }
}
export default App;
